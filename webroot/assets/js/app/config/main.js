require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });

        // data
        app.city = [
            {
                id: 0,
                value: '东区',
                childs:[
                    {id: 1, value: '杭州'},
                    {id: 2, value: '上海'},
                    {id: 3, value: '西安'},
                    {id: 4, value: '兰州'},
                    {id: 5, value: '银川'},
                    {id: 6, value: '西宁'},
                    {id: 7, value: '嘉兴'},
                    {id: 8, value: '湖州'},
                    {id: 9, value: '金华'}
                ]
            },
            {
                id: 1,
                value:'东北区',
                childs: [
                    {id: 1, value: '温州'},
                    {id: 2, value: '宁波'},
                    {id: 3, value: '绍兴'},
                    {id: 4, value: '沈阳'},
                    {id: 5, value: '大庆'},
                    {id: 6, value: '抚顺'},
                    {id: 7, value: '沈阳'},
                    {id: 8, value: '哈尔滨'},
                    {id: 9, value: '长春'},
                    {id: 10, value: '郑州'},
                    {id: 11, value: '洛阳'},
                    {id: 12, value: '内蒙古'}
                ]
            },
            {
                id: 2,
                value:'中区',
                childs: [
                    {id: 1, value: '南京'},
                    {id: 2, value: '芜湖'},
                    {id: 3, value: '徐州'},
                    {id: 4, value: '镇江'},
                    {id: 5, value: '马鞍山'},
                    {id: 6, value: '南通'},
                    {id: 7, value: '盐城'},
                    {id: 8, value: '连云港'},
                    {id: 9, value: '青岛'},
                    {id: 10, value: '威海'},
                    {id: 11, value: '烟台'},
                    {id: 12, value: '济南'},
                    {id: 13, value: '济宁'},
                    {id: 14, value: '潍坊'},
                    {id: 15, value: '淄博'},
                    {id: 16, value: '合肥'},
                    {id: 17, value: '常州'},
                    {id: 18, value: '蚌埠'},
                    {id: 19, value: '无锡'},
                    {id: 20, value: '泰州'},
                    {id: 21, value: '常熟'},
                    {id: 22, value: '昆山'},
                    {id: 23, value: '大连'},
                    {id: 24, value: '江阴'},
                    {id: 25, value: '苏州'}
                ]
            },
            {
                id: 3,
                value:'南区',
                childs: [
                    {id: 1, value: '广州'},
                    {id: 2, value: '珠海'},
                    {id: 3, value: '深圳'},
                    {id: 4, value: '佛山'},
                    {id: 5, value: '湛江'},
                    {id: 6, value: '中山'},
                    {id: 7, value: '东莞'},
                    {id: 8, value: '汕头'},
                    {id: 9, value: '武汉'},
                    {id: 10, value: '长沙'},
                    {id: 11, value: '湘潭'},
                    {id: 12, value: '株洲'},
                    {id: 13, value: '南昌'},
                    {id: 14, value: '厦门'},
                    {id: 15, value: '泉州'},
                    {id: 16, value: '南宁'},
                    {id: 17, value: '福州'}
                ]
            },
            {
                id: 4,
                value:'西区',
                childs: [
                    {id: 1, value: '重庆'},
                    {id: 2, value: '贵阳'},
                    {id: 3, value: '昆明'},
                    {id: 4, value: '成都'},
                    {id: 5, value: '泸州'},
                    {id: 6, value: '绵阳'}
                ]
            },
            {
                id: 5,
                value:'北区',
                childs: [
                    {id: 1, value: '北京'},
                    {id: 2, value: '天津'},
                    {id: 3, value: '唐山'},
                    {id: 4, value: '廊坊'},
                    {id: 5, value: '太原'},
                    {id: 6, value: '石家庄'},
                    {id: 7, value: '乌鲁木齐'},
                    {id: 8, value: '保定'}
                ]
            }
        ];
        
        // teac
        app.teac = [
            ['吴芳', '万丽', '黄贝', '周群', '刘瑛', '谭忠飏', '龚新娟', '李梦迪', '宋海涛', '王理'],
            ['周伟', '邱彩秀', '王娜', '王运凯'],
            ['周亚玲', '徐晶', '于承珠', '刘翠苹', '朱见晶', '周章忠', '吕晓丽', '张琨', '郑欢欢'],
            ['廖舒婷', '倪海', '洪静玲', '徐荣', '俞莉莎', '徐果', '陈敏', '陈思', '周萃玲'],
            ['何小凤', '陈彦男', '彭媛', '哈里丹'],
            ['刘依娜', '刘颖', '李润沛', '惠康蓉', '张强', '谷月']
        ];

        app.searchTeac = [
            {
                id: 0,
                value: '东区',
                childs:[
                    {id: 1, value: '吴芳'},
                    {id: 2, value: '万丽'},
                    {id: 3, value: '黄贝'},
                    {id: 4, value: '周群'},
                    {id: 5, value: '刘瑛'},
                    {id: 6, value: '谭忠飏'},
                    {id: 7, value: '龚新娟'},
                    {id: 8, value: '李梦迪'},
                    {id: 9, value: '宋海涛'},
                    {id: 10, value: '王理'}
                ]
            },
            {
                id: 1,
                value:'东北区',
                childs: [
                    {id: 1, value: '周伟'},
                    {id: 2, value: '邱彩秀'},
                    {id: 3, value: '王娜'},
                    {id: 4, value: '王运凯'}
                ]
            },
            {
                id: 2,
                value:'中区',
                childs: [
                    {id: 1, value: '周亚玲'},
                    {id: 2, value: '徐晶'},
                    {id: 3, value: '于承珠'},
                    {id: 4, value: '刘翠苹'},
                    {id: 5, value: '朱见晶'},
                    {id: 6, value: '周章忠'},
                    {id: 7, value: '吕晓丽'},
                    {id: 8, value: '张琨'},
                    {id: 9, value: '郑欢欢'}
                ]
            },
            {
                id: 3,
                value:'南区',
                childs: [
                    {id: 1, value: '廖舒婷'},
                    {id: 2, value: '倪海'},
                    {id: 3, value: '洪静玲'},
                    {id: 4, value: '徐荣'},
                    {id: 5, value: '俞莉莎'},
                    {id: 6, value: '徐果'},
                    {id: 7, value: '陈敏'},
                    {id: 8, value: '陈思'},
                    {id: 9, value: '周萃玲'}
                ]
            },
            {
                id: 4,
                value:'西区',
                childs: [
                    {id: 1, value: '何小凤'},
                    {id: 2, value: '陈彦男'},
                    {id: 3, value: '彭媛'},
                    {id: 4, value: '哈里丹'}
                ]
            },
            {
                id: 5,
                value:'北区',
                childs: [
                    {id: 1, value: '刘依娜'},
                    {id: 2, value: '刘颖'},
                    {id: 3, value: '李润沛'},
                    {id: 4, value: '惠康蓉'},
                    {id: 5, value: '张强'},
                    {id: 6, value: '谷月'}
                ]
            }
        ];

        // app.listTeac = ['吴芳', '万丽', '黄贝', '周群', '刘瑛', '谭忠飏', '龚新娟', '李梦迪', '宋海涛', '王理', '周伟', '邱彩秀', '王娜', '王运凯', '周亚玲', '徐晶', '于承珠', '刘翠苹', '朱见晶', '周章忠', '吕晓丽', '张琨', '郑欢欢', '廖舒婷', '倪海', '洪静玲', '徐荣', '俞莉莎', '徐果', '陈敏', '陈思', '周萃玲', '何小凤', '陈彦男', '彭媛', '哈里丹', '刘依娜', '刘颖', '李润沛', '惠康蓉', '张强', '谷月'];

        // app.listArea = ['杭州', '上海', '西安', '兰州', '银川', '西宁', '嘉兴', '湖州', '金华', '大连', '青岛', '温州', '宁波', '绍兴', '沈阳', '大庆', '抚顺', '沈阳', '哈尔滨', '长春', '郑州', '洛阳', '内蒙古', '南京', '芜湖', '徐州', '镇江', '马鞍山', '南通', '盐城', '连云港', '青岛', '威海威', '烟台', '济南', '济宁', '潍坊', '淄博', '合肥', '常州', '蚌埠', '无锡', '泰州', '常熟', '昆山', '大连', '江阴', '苏州', '广州', '珠海', '深圳', '佛山', '湛江', '中山', '东莞', '汕头', '武汉', '长沙', '湘潭', '株洲', '南昌', '厦门', '泉州', '南宁', '福州', '昆明', '重庆', '贵阳', '成都', '泸州', '绵阳', '北京', '天津', '唐山', '廊坊', '太原', '石家庄', '乌鲁木齐', '保定'];

        app.listTeac = [];
        app.listArea = [];
        app.listNum = [];
        app.listStatus = [];

        // 微信不分享
        // function onBridgeReady() { 
        //     WeixinJSBridge.call('hideOptionMenu'); 
        // };
        // if (typeof WeixinJSBridge == "undefined") { 
        //     if (document.addEventListener) { 
        //         document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false); 
        //     } else if (document.attachEvent) { 
        //         document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        //         document.attachEvent('onWeixinJSBridgeReady', onBridgeReady); 
        //     } 
        // } else { 
        //     onBridgeReady(); 
        // };

        // // 微信分享api

        // var shareData = {
        //     title: 'JJ真爱粉大联考',
        //     desc: 'get林俊杰《圣所2.0》世界巡回演唱会新加坡站全球最后2张票+往返机票！',
        //     link: 'http://changijjlin.gypserver.com',
        //     imgUrl: 'http://changijjlin.gypserver.com/assets/images/share.jpg',
        //     type:'',
        //     dataUrl:'',
        //     success: function() {
        //         // console.log('-----success'+ shareData.link)
        //     },
        //     error: function() {
        //         //
        //     },
        //     cancel: function () { 
        //       // 用户取消分享后执行的回调函数
        //     }
        // };

        // function loadJs(url,callback){
        //     var script=document.createElement('script');
        //     script.type="text/javascript";
        //     if(typeof(callback)!="undefined"){
        //         if(script.readyState){
        //             script.onreadystatechange=function(){
        //                 if(script.readyState == "loaded" || script.readyState == "complete"){
        //                     script.onreadystatechange=null;
        //                     callback();
        //                 }
        //             }
        //         }else{
        //             script.onload=function(){
        //                 callback();
        //             }
        //         }
        //     }
        //     script.src=url;
        //     document.body.appendChild(script);
        // };

        // function wxshare(){
        //     loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
        //         // console.log('wx_conf' + JSON.stringify(wx_conf))
        //         wx.config({
        //             debug: false,
        //             appId: wx_conf.appId,
        //             timestamp: wx_conf.timestamp,
        //             nonceStr: wx_conf.nonceStr,
        //             signature: wx_conf.signature,
        //             jsApiList: [
        //                 'checkJsApi',
        //                 'onMenuShareTimeline',
        //                 'onMenuShareAppMessage'
        //             ]
        //         });   
        //     });
        //     wx.ready(function() {
        //         // console.log('wxshare' + JSON.stringify(shareData))
        //         wx.onMenuShareAppMessage(shareData);
        //         wx.onMenuShareTimeline(shareData);    
        //     });  
        //     wx.error(function(res){
        //         console.log('出错了'+JSON.stringify(res))
        //         loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
        //             // console.log('wx_conf' + JSON.stringify(wx_conf))
        //             wx.config({
        //                 debug: false,
        //                 appId: wx_conf.appId,
        //                 timestamp: wx_conf.timestamp,
        //                 nonceStr: wx_conf.nonceStr,
        //                 signature: wx_conf.signature,
        //                 jsApiList: [
        //                     'checkJsApi',
        //                     'onMenuShareTimeline',
        //                     'onMenuShareAppMessage'
        //                 ]
        //             });   
        //         });
        //     });
        // };
        // wxshare();
        
    });