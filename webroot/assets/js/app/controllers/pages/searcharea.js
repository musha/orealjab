// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'searcharea');
            };

            // fill area
            var teacSelect = new MobileSelect({
                trigger: '.teacName',
                title: '培训师选择',
                wheels: [
                            {data:[
                                {
                                    id: '', 
                                    value: '',
                                    childs: [
                                        {id: '', value: ''},
                                    ]
                                }
                            ]}
                        ],
                transitionEnd:function(indexArr, data){
                    // console.log(data);
                },
                callback:function(indexArr, data){
                    $('.teacName').text(data[1].value);
                }
            });
            teacSelect.updateWheels(app.searchTeac); // Update the wheel

            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.searcharea_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // check number
            function checkTeac(teac) {
                if(teac == '' || teac == null || teac == undefined) {
                    alert('请选择培训师');
                    return false;
                } else {
                    return true;
                }
            };

            // search
            $('.search').on('click', function() {
                if(checkTeac(teacSelect.getValue())) {
                    sessionStorage.setItem('teacName', teacSelect.getValue()[1].value);
                    tl.kill();
                    app.router.goto('listarea');
                };
            });

            // all
            $('.btn-all').on('click', function() {
                tl.kill();
                app.router.goto('listall');
            });
        }
    })
    //  Return the module for AMD compliance.
    return Page;
});
