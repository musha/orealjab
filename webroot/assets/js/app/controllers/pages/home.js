// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'home');
            };

            // 没登录则隐藏
            if((localStorage.getItem('userName') == null || localStorage.getItem('userName') == undefined) || (localStorage.getItem('userNum') == null || localStorage.getItem('userNum') == undefined)) {
                $('.out').hide();
            };

            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.home_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // to study
            $('.btn-study').on('click', function() {
                tl.kill();
                if((localStorage.getItem('userName') != null || localStorage.getItem('userName') != undefined) && (localStorage.getItem('userNum') != null || localStorage.getItem('userNum') != undefined)) {
                    // 有用户缓存，则不用登录
                    app.router.goto('modules');
                } else {
                    // 需要登录
                    app.router.goto('login');
                };
            });

            // to check
            $('.btn-check').on('click', function() {
                tl.kill();
                app.router.goto('check');
            });

            // out 
            $('.out').on('click', function() {
                $('.modal-out').fadeIn('fast');
            });

            // y
            var canOut = 1; // 是否可以退出，默认可以
            $('.btn-y').off('click');
            $('.btn-y').on('click', function() {
                if((localStorage.getItem('userName') == null || localStorage.getItem('userName') == undefined) || (localStorage.getItem('userNum') == null || localStorage.getItem('userNum') == undefined)) {
                    alert('用户已登出');
                } else {
                    if(canOut == 1) {
                        // 正在调接口，不可以重复调取
                        canOut = 0;
                        $('.modal-loading').fadeIn('fast');
                        $.ajax({
                            type: 'post',
                            url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ExitLogin',
                            dataType: 'json',
                            // contentType: 'application/json',
                            data: {},
                            // async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);
                                if(data.msg != '') {
                                    alert(data.msg);
                                };
                                if(data.status) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    localStorage.removeItem('studyStatus'); 
                                    localStorage.removeItem('level1score');
                                    localStorage.removeItem('level2score');
                                    localStorage.removeItem('level3score');
                                    localStorage.removeItem('level1prize');
                                    localStorage.removeItem('level2prize');
                                    localStorage.removeItem('level3prize');
                                    localStorage.removeItem('level1prizeEx');
                                    localStorage.removeItem('level2prizeEx');
                                    localStorage.removeItem('level3prizeEx');
                                    // localStorage.removeItem('percentA');
                                    // localStorage.removeItem('percentB');
                                    // localStorage.removeItem('percentL');
                                    // localStorage.removeItem('percentS');
                                    $('.out').hide();
                                    $('.modal-out').fadeOut('fast');
                                };
                            },
                            error: function() {
                                //
                                alert('登出失败，请重试');
                            },
                            complete: function(data) {
                                // 调取完毕
                                canOut = 1;
                                $('.modal-loading').fadeOut('fast');
                            }
                        });
                    };
                };
            });

             // n
            $('.btn-n').on('click', function() {
                $('.modal-out').fadeOut('fast');
            });
        }
    })
    //  Return the module for AMD compliance.
    return Page;
});
