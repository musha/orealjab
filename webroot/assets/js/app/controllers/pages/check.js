// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'check');
            };

            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.check_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // to shop
            $('.shop').on('click', function() {
                tl.kill();
                app.router.goto('searchshop');
            });

            // to area
            $('.area').on('click', function() {
                tl.kill();
                app.router.goto('searcharea');
            });

            //back home
            $('.btn-home').on('click', function() {
                tl.kill();
                app.router.goto('home');
            });
        }
    })
    //  Return the module for AMD compliance.
    return Page;
});
