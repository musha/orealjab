// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'test');
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.test_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                // 
            } }, 0.1);

            // test
            if(JSON.parse(localStorage.getItem('level1score')) >= 80) {
                // 过了level1
                $('.level2').addClass('unlock');
                $('.level2').attr('src', 'assets/images/test/btn21.png');
                if(JSON.parse(localStorage.getItem('level2score')) >= 3.5) {
                    // 过了level2
                    $('.level3').addClass('unlock');
                    $('.level3').attr('src', 'assets/images/test/btn31.png');
                };
            };

            // level1
            $('.level1.unlock').on('click', function() {
                if(JSON.parse(localStorage.getItem('level1score')) < 100) {
                    tl.kill();
                    app.router.goto('test1', ['test']);
                } else {
                    alert('您已通过测试');
                }; 
            });

            // level2
            $('.level2.unlock').on('click', function() {
                if(JSON.parse(localStorage.getItem('level2score')) < 5) {
                    tl.kill();
                    app.router.goto('test2', ['test']);
                } else {
                    alert('您已通过测试');
                };
            });

            // level3
            $('.level3.unlock').on('click', function() {
                if(JSON.parse(localStorage.getItem('level3score')) < 100) {
                    tl.kill();
                    app.router.goto('test3', ['test']);
                } else {
                    alert('您已通过测试');
                };
            });

            // back
            $('.btn-back').on('click', function() {
                tl.kill();
                app.router.goto('modules');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
