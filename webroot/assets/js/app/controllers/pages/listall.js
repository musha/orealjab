// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'listall');
            };

            // 获取最新区域进度
            $('.screen-item select').prop('disabled', true);
            $('.modal-loading').fadeIn('fast');
            $.ajax({
                type: 'post',
                url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetAllProgress',
                dataType: 'json',
                // contentType: 'application/json',
                data: {
                    trainer: '',
                    regional: '',
                    storeNum: '',
                    status: ''
                },
                // async: false,
                cache: false,
                success: function(data) {
                    console.log(data);
                    // if(data.msg != '') {
                    //     alert(data.msg);
                    // };
                    if(data.status) {
                        var len = data.data.length,
                            itemTeac,
                            itemArea,
                            itemNum,
                            itemStatus;
                        if(len > 0) {
                            // 有数据
                            for(var i = 0; i < len; i++) {
                                var status = '--';
                                switch(data.data[i].StudyStatus) {
                                    case 0:
                                        break;
                                    case 1:
                                        status = '在读';
                                        break;
                                    case 2:
                                        status = '复读';
                                        break;
                                    case 3:
                                        status = '毕业';
                                        break;
                                    default:
                                        //   
                                };
                                var $div = $('<div class="list-item"><div class="teac text-center">' + data.data[i].Trainer + '</div><div class="area">' + data.data[i].City + '</div><div class="num text-center">' + data.data[i].StoreNum + '</div><div class="name text-center">' + data.data[i].Name + '</div><div class="list-score"><div class="wrapper-score rel"><div class="score-item score-level1">种子期<span>' + data.data[i].EntryLevelScore + '</span><i class="icon-arrow"></i></div><div class="score-more abs full-w"><div class="score-item score-level2">成长期<span>' + data.data[i].AdvancedScore.toFixed(2) + '</span></div><div class="score-item score-level3">成熟期<span>' + data.data[i].MatureScore + '</span></div></div></div></div><div class="list-percent"><div class="wrapper-percent rel"><div class="percent-item percent1">兰&nbsp;&nbsp;&nbsp;蔻<span>' + data.data[i].LancomeRatio + '%</span><i class="icon-arrow"></i></div><div class="percent-more abs full-w"><div class="percent-item percent2">植村秀<span>' + data.data[i].ShuUemuraRatio + '%</span></div><div class="percent-item percent3">碧欧泉<span>' + data.data[i].BiothermRatio + '%</span></div><div class="percent-item percent4">阿玛尼<span>' + data.data[i].AmaniRatio + '%</span></div></div></div></div><div class="status text-center">' + status + '</div></div>');
                                $('.table-list').append($div);
                                itemTeac = data.data[i].Trainer;
                                if(app.listTeac.indexOf(itemTeac) == -1) {
                                    app.listTeac.push(itemTeac);
                                };
                                itemArea = data.data[i].City;
                                if(app.listArea.indexOf(itemArea) == -1) {
                                    app.listArea.push(itemArea);
                                };
                                itemNum = data.data[i].StoreNum;
                                if(app.listNum.indexOf(itemNum) == -1) {
                                    app.listNum.push(itemNum);
                                };
                                itemStatus = data.data[i].StudyStatus;
                                if(app.listStatus.indexOf(itemStatus) == -1) {
                                    app.listStatus.push(itemStatus);
                                };
                            };
                        } else {
                            $('.nodata').fadeIn('fast');
                        };
                    };
                },
                error: function() {
                    //
                    alert('获取进度失败，请刷新重试');
                },
                complete: function(data) {
                    // fill teac
                    if(app.listTeac.length > 0) {
                        if($('.select-teac .defaultOption').siblings().length == 0) {
                            var len = app.listTeac.length;
                            for(var i = 0; i < len; i++) {
                                $('.select-teac').append('<option value=' + app.listTeac[i] + '>' + app.listTeac[i] + '</option>');
                            };
                        };
                    };
                    // fill area
                    if(app.listArea.length > 0) {
                        if($('.select-area .defaultOption').siblings().length == 0) {
                            var len = app.listArea.length;
                            for(var i = 0; i < len; i++) {
                                $('.select-area').append('<option value=' + app.listArea[i] + '>' + app.listArea[i] + '</option>');
                            };
                        };
                    };
                    // fill num
                    if(app.listNum.length > 0) {
                        if($('.select-num .defaultOption').siblings().length == 0) {
                            var len = app.listNum.length;
                            for(var i = 0; i < len; i++) {
                                $('.select-num').append('<option value=' + app.listNum[i] + '>' + app.listNum[i] + '</option>');
                            };
                        };
                    };
                    // fill num
                    if(app.listStatus.length > 0) {
                        if($('.select-status .defaultOption').siblings().length == 0) {
                            var len = app.listStatus.length;
                            for(var i = 0; i < len; i++) {
                                var status = '--';
                                switch(app.listStatus[i]) {
                                    case 0:
                                        break;
                                    case 1:
                                        status = '在读';
                                        break;
                                    case 2:
                                        status = '复读';
                                        break;
                                    case 3:
                                        status = '毕业';
                                        break;
                                    default:
                                        //   
                                };
                                $('.select-status').append('<option value=' + app.listStatus[i] + '>' + status + '</option>');
                            };
                        };
                    };
                    $('.screen-item select').prop('disabled', false);
                    $('.modal-loading').fadeOut('fast');
                    // score
                    $('.score-level1').on('click', function() {
                        $(this).next().toggleClass('show');
                    });

                    // percent
                    $('.percent1').on('click', function() {
                        $(this).next().toggleClass('show');
                    });
                }
            }); 

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.listall_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // 筛选
            // 选出当前最新层级
            function currentMax() {
                var max = 0;
                for(var i = 0; i < $('.screen-item select').length; i++) {
                    if($($('.screen-item select')[i]).data('index') > max) {
                        max = $($('.screen-item select')[i]).data('index');
                    };
                };
                return max;
            };
            $('.screen-item select').on('input propertychange', function() {
                $('.screen-item select').prop('disabled', true);
                $('.modal-loading').fadeIn('fast');
                var that = $(this);
                max = currentMax();
                if(that.data('index') > 0) {
                    // 已经选过了
                    if(that.data('index') < max) {
                        // 高于当前最新级别 后面的层级都重置 
                        for(var i = 0; i < $('.screen-item select').length; i++) {
                            if($($('.screen-item select')[i]).data('index') > $(this).data('index')) {
                                $($('.screen-item select')[i]).data('index', 0);
                                $($('.screen-item select')[i]).val('');
                            };
                        };
                    };
                    if(that.val() == '') {
                        // 全部
                        that.data('index', 0);
                    };
                } else {
                    // 还没有被选过
                    that.data('index', (max + 1));
                };
                $.ajax({
                    type: 'post',
                    url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetAllProgress',
                    dataType: 'json',
                    // contentType: 'application/json',
                    data: {
                        trainer: $('.select-teac').val(),
                        regional: $('.select-area').val(),
                        storeNum: $('.select-num').val(),
                        status: $('.select-status').val()
                    },
                    // async: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        // if(data.msg != '') {
                        //     alert(data.msg);
                        // };
                        if(data.status) {
                            $('.table-list .nodata').siblings().remove();
                            var len = data.data.length,
                                itemTeac,
                                itemArea,
                                itemNum,
                                itemStatus;
                            for(var i = 0; i < $('.screen-item select').length; i++) {
                                if(($($('.screen-item select')[i]).attr('id') != that.attr('id')) && $($('.screen-item select')[i]).data('index') == 0) {
                                    switch($($('.screen-item select')[i]).attr('id')) {
                                        case 'teac':
                                            app.listTeac = [];
                                            break;
                                        case 'area':
                                            app.listArea = [];
                                            break;
                                        case 'num':
                                            app.listNum = [];
                                            break;
                                        case 'status':
                                            app.listStatus = [];
                                            break;
                                        default:
                                            //
                                    };
                                };
                            };
                            if(len > 0) {
                                // 有数据
                                $('.nodata').fadeOut('fast');
                                for(var i = 0; i < len; i++) {
                                    var status = '--';
                                    switch(data.data[i].StudyStatus) {
                                        case 0:
                                            break;
                                        case 1:
                                            status = '在读';
                                            break;
                                        case 2:
                                            status = '复读';
                                            break;
                                        case 3:
                                            status = '毕业';
                                            break;
                                        default:
                                            //   
                                    };
                                    var $div = $('<div class="list-item"><div class="teac text-center">' + data.data[i].Trainer + '</div><div class="area">' + data.data[i].City + '</div><div class="num text-center">' + data.data[i].StoreNum + '</div><div class="name text-center">' + data.data[i].Name + '</div><div class="list-score"><div class="wrapper-score rel"><div class="score-item score-level1">种子期<span>' + data.data[i].EntryLevelScore + '</span><i class="icon-arrow"></i></div><div class="score-more abs full-w"><div class="score-item score-level2">成长期<span>' + data.data[i].AdvancedScore.toFixed(2) + '</span></div><div class="score-item score-level3">成熟期<span>' + data.data[i].MatureScore + '</span></div></div></div></div><div class="list-percent"><div class="wrapper-percent rel"><div class="percent-item percent1">兰&nbsp;&nbsp;&nbsp;蔻<span>' + data.data[i].LancomeRatio + '%</span><i class="icon-arrow"></i></div><div class="percent-more abs full-w"><div class="percent-item percent2">植村秀<span>' + data.data[i].ShuUemuraRatio + '%</span></div><div class="percent-item percent3">碧欧泉<span>' + data.data[i].BiothermRatio + '%</span></div><div class="percent-item percent4">阿玛尼<span>' + data.data[i].AmaniRatio + '%</span></div></div></div></div><div class="status text-center">' + status + '</div></div>');
                                    $('.table-list').append($div);
                                    if(($('.select-teac').attr('id') != that.attr('id')) && $('.select-teac').data('index') == 0) {
                                        itemTeac = data.data[i].Trainer;
                                        if(app.listTeac.indexOf(itemTeac) == -1) {
                                            app.listTeac.push(itemTeac);
                                        };
                                    };
                                    if(($('.select-area').attr('id') != that.attr('id')) && $('.select-area').data('index') == 0)  {
                                        itemArea = data.data[i].City;
                                        if(app.listArea.indexOf(itemArea) == -1) {
                                            app.listArea.push(itemArea);
                                        };
                                    };
                                    if(($('.select-num').attr('id') != that.attr('id')) && $('.select-num').data('index') == 0) {
                                        itemNum = data.data[i].StoreNum;
                                        if(app.listNum.indexOf(itemNum) == -1) {
                                            app.listNum.push(itemNum);
                                        };
                                    };
                                    if(($('.select-status').attr('id') != that.attr('id')) && $('.select-status').data('index') == 0) {
                                        itemStatus = data.data[i].StudyStatus;
                                        if(app.listStatus.indexOf(itemStatus) == -1) {
                                            app.listStatus.push(itemStatus);
                                        };
                                    };
                                };
                            } else {
                                $('.nodata').fadeIn('fast');
                            };
                        }
                    },
                    error: function() {
                        //
                        alert('获取进度失败，请刷新重试');
                    },
                    complete: function(data) {
                        // fill teac
                        if(($('.select-teac').attr('id') != that.attr('id')) && $('.select-teac').data('index') == 0) {
                            if(app.listTeac.length > 0) {
                                $('.select-teac .defaultOption').siblings().remove();
                                var len = app.listTeac.length;
                                for(var i = 0; i < len; i++) {
                                    $('.select-teac').append('<option value=' + app.listTeac[i] + '>' + app.listTeac[i] + '</option>');
                                };
                            };
                        };
                        // fill area
                        if(($('.select-area').attr('id') != that.attr('id')) && $('.select-area').data('index') == 0) {
                            if(app.listArea.length > 0) {
                                $('.select-area .defaultOption').siblings().remove();
                                var len = app.listArea.length;
                                for(var i = 0; i < len; i++) {
                                    $('.select-area').append('<option value=' + app.listArea[i] + '>' + app.listArea[i] + '</option>');
                                };
                            };
                        };
                        // fill num
                        if(($('.select-num').attr('id') != that.attr('id')) && $('.select-num').data('index') == 0) {
                            if(app.listNum.length > 0) {
                                $('.select-num .defaultOption').siblings().remove();
                                var len = app.listNum.length;
                                for(var i = 0; i < len; i++) {
                                    $('.select-num').append('<option value=' + app.listNum[i] + '>' + app.listNum[i] + '</option>');
                                };
                            };
                        };
                        // fill status
                        if(($('.select-status').attr('id') != that.attr('id')) && $('.select-status').data('index') == 0) {
                            if(app.listStatus.length > 0) {
                                $('.select-status .defaultOption').siblings().remove();
                                var len = app.listStatus.length;
                                for(var i = 0; i < len; i++) {
                                    var status = '--';
                                    switch(app.listStatus[i]) {
                                        case 0:
                                            break;
                                        case 1:
                                            status = '在读';
                                            break;
                                        case 2:
                                            status = '复读';
                                            break;
                                        case 3:
                                            status = '毕业';
                                            break;
                                        default:
                                            //   
                                    };
                                    $('.select-status').append('<option value=' + app.listStatus[i] + '>' + status + '</option>');
                                };
                            }; 
                        };
                        $('.screen-item select').prop('disabled', false);
                        $('.modal-loading').fadeOut('fast');
                        // score
                        $('.score-level1').on('click', function() {
                            $(this).next().toggleClass('show');
                        });

                        // percent
                        $('.percent1').on('click', function() {
                            $(this).next().toggleClass('show');
                        });
                    }
                });
            });

            // back check
            $('.btn-check').on('click', function() {
                tl.kill();
                app.router.goto('check');
            });
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
