// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'analyse1'); 
            };

            // fill
            $('.score').text(JSON.parse(localStorage.getItem('level1score')));
            $('.right-count').text(JSON.parse(sessionStorage.getItem('rightNum1')));
            $('.wrong-count').text(JSON.parse(sessionStorage.getItem('wrongNum1')));
            var anAll = JSON.parse(sessionStorage.getItem('anAll')),
                wrongArr = JSON.parse(sessionStorage.getItem('errorArr1')).split(',');
            // console.log(anAll);
            // console.log(wrongArr);
            if(JSON.parse(sessionStorage.getItem('wrongNum1')) > 0) {
                // 有错题
                var wrongNum = wrongArr.length;
                for(var i = 0; i < wrongNum; i++) {
                    $('.qa-item' + wrongArr[i]).addClass('show');
                    var len = $('.q' + wrongArr[i] + ' .an').length;
                    if(len > 1) {
                        // 填空
                        for(var j = 0; j < len; j++) {
                            if(anAll[(wrongArr[i] - 1)] != '') {
                                $($('.q' + wrongArr[i] + ' .an')[j]).text((anAll[(wrongArr[i] - 1)].replace(/,/g, ''))[j]);
                            };
                        };
                    } else {
                        // 选择
                        if(anAll[(wrongArr[i] - 1)] != '') {
                            $($('.q' + wrongArr[i] + ' .an')[0]).text(anAll[(wrongArr[i] - 1)].replace(/,/g, ''));
                        };
                    };
                };
            };

            // 根据分数判断按钮是否点亮
            if(JSON.parse(localStorage.getItem('level1score')) < 80) {
                // 没过
                $('.btn-retest').addClass('again');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest1.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass0.png');
            } else if(JSON.parse(localStorage.getItem('level1score')) >= 80 && JSON.parse(localStorage.getItem('level1score')) < 100) {
                // 过了不是满分
                $('.btn-retest').addClass('again');
                $('.btn-pass').addClass('pass');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest1.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass1.png');
            } else {
                // 满分
                $('.btn-pass').addClass('pass');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest0.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass1.png');
            }
             
            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.analyse1_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // retest
            $('.btn-retest.again').on('click', function() {
                tl.kill();
                sessionStorage.setItem('fromAnalyse1', true);
                app.router.goto('guide1');
            });

            // pass
            $('.btn-pass.pass').on('click', function() {
                tl.kill();
                app.router.goto('prize');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
