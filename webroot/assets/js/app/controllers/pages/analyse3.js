// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'analyse3'); 
            };

            // fill
            $('.score').text(JSON.parse(localStorage.getItem('level3score')));
            $('.right-count').text(JSON.parse(sessionStorage.getItem('rightNum3')));
            $('.wrong-count').text(JSON.parse(sessionStorage.getItem('wrongNum3')));
            var wrongArr = JSON.parse(sessionStorage.getItem('errorArr3'));
            if(JSON.parse(sessionStorage.getItem('wrongNum3')) > 0) {
                // 有错题
                var len = wrongArr.length;
                for(var i = 0; i < len; i++) {
                    $('.qa-item' + wrongArr[i]).addClass('show');
                };
            };

            // 根据分数判断按钮是否点亮
            if(JSON.parse(localStorage.getItem('level3score')) < 100) {
                // 没过
                $('.btn-retest').addClass('again');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest1.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass0.png');
            }  else {
                // 满分
                $('.btn-pass').addClass('pass');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest0.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass1.png');
            };
             
            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.analyse3_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // retest
            $('.btn-retest.again').on('click', function() {
                tl.kill();
                sessionStorage.setItem('fromAnalyse3', true);
                app.router.goto('guide3');
            });

            // pass
            $('.btn-pass.pass').on('click', function() {
                tl.kill();
                app.router.goto('prize');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
