// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'searchshop');
            };

            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.searchshop_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // check number
            function checkNumber(num) {
                if(num == '') {
                    alert('请输入店铺编号');
                    return false;
                };
                var reg = /^\d{4}$/;
                if (!reg.test(num)) {
                    alert('请输入4位店铺编号');
                    return false;
                } else {
                    return true;
                }
            };

            // search
            $('.search').on('click', function() {
                if(checkNumber($('.shopNum').val())) {
                    sessionStorage.setItem('shopNum', $('.shopNum').val());
                    tl.kill();
                    app.router.goto('listshop');
                };
            });

            //back check
            $('.btn-check').on('click', function() {
                tl.kill();
                app.router.goto('check');
            });
        }
    })
    //  Return the module for AMD compliance.
    return Page;
});
