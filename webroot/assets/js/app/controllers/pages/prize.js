// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'prize');
            };

            var context = this;

            // prize
            if(JSON.parse(localStorage.getItem('level1score')) >= 80) {
                // 过了level1
                $('.level1').removeClass('lock');
                $('.level1').addClass('unlock');
                if(JSON.parse(localStorage.getItem('level1prize'))) {
                    // 领过奖了
                    $('.level1').addClass('applyed');
                    $('.level1 .box-apply').removeClass('apply');
                    $('.level1 .box-apply img').attr('src', 'assets/images/prize/success-apply.png');
                    if(JSON.parse(localStorage.getItem('level1prizeEx'))) {
                        // 兑换过了
                        $('.level1 .box-exchange').removeClass('exchange');
                        $('.level1 .box-exchange img').attr('src', 'assets/images/prize/success-exchange.png');
                    };        
                };
                if(JSON.parse(localStorage.getItem('level2score')) >= 3.5) {
                    // 过了level2
                    $('.level2').removeClass('lock');
                    $('.level2').addClass('unlock');
                    if(JSON.parse(localStorage.getItem('level2prize'))) {
                        // 领过奖了
                        $('.level2').addClass('applyed');
                        $('.level2 .box-apply').removeClass('apply');
                        $('.level2 .box-apply img').attr('src', 'assets/images/prize/success-apply.png');
                        if(JSON.parse(localStorage.getItem('level2prizeEx'))) {
                            // 兑换过了
                            $('.level2 .box-exchange').removeClass('exchange');
                            $('.level2 .box-exchange img').attr('src', 'assets/images/prize/success-exchange.png');
                        }
                    };
                    if(JSON.parse(localStorage.getItem('level3score')) == 100) {
                        // 过了level3
                        $('.level3').removeClass('lock');
                        $('.level3').addClass('unlock');
                        if(JSON.parse(localStorage.getItem('level3prize'))) {
                            // 领过奖了
                            $('.level3').addClass('applyed');
                            $('.level3 .box-apply').removeClass('apply');
                            $('.level3 .box-apply img').attr('src', 'assets/images/prize/success-apply.png');
                            if(JSON.parse(localStorage.getItem('level3prizeEx'))) {
                                // 兑换过了
                                $('.level3 .box-exchange').removeClass('exchange');
                                $('.level3 .box-exchange img').attr('src', 'assets/images/prize/success-exchange.png');
                            }
                        };
                    };
                };
            };

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.prize_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // apply & exchange
            var canApply1 = 1;
            $('.level1.unlock .box-apply.apply').off('click');
            $('.level1.unlock .box-apply.apply').on('click', function() {
                if(JSON.parse(localStorage.getItem('level1prize'))) {
                    alert('奖品已申请');
                    return;
                };
                if(canApply1 == 1) {
                    // 调取中，不可重复调取
                    canApply1 = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ReceiveAward',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 1
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('level1prize', JSON.stringify(data.data.EntryLevelPrize));
                                $('.level1').addClass('applyed');
                                $('.level1 .box-apply').removeClass('apply');
                                $('.level1 .box-apply img').attr('src', 'assets/images/prize/success-apply.png');
                            };
                        },
                        error: function() {
                            //
                            alert('申请失败，请重试');
                        },
                        complete: function(data) {
                            // 调取结束
                            canApply1 = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    }); 
                };
            });
            var canExchange1 = 1;
            $('.level1 .box-exchange.exchange').off('click');
            $('.level1 .box-exchange.exchange').on('click', function() {
                if(JSON.parse(localStorage.getItem('level1prizeEx'))) {
                    alert('奖品已兑换');
                    return;
                };
                if(canExchange1 == 1) {
                    // 调取中，不可重复调取
                    canExchange1 = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ExchangeAward',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 1
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('level1prizeEx', JSON.stringify(data.data.EntryLevelExchangePrize));
                                $('.level1 .box-exchange').removeClass('exchange');
                                $('.level1 .box-exchange img').attr('src', 'assets/images/prize/success-exchange.png');
                            };
                        },
                        error: function() {
                            //
                            alert('兑换失败，请重试');
                        },
                        complete: function(data) {
                            // 调取结束
                            canExchange1 = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    }); 
                };
            });

            var canApply2 = 1;
            $('.level2.unlock .apply').off('click');
            $('.level2.unlock .apply').on('click', function() {
                if(JSON.parse(localStorage.getItem('level2prize'))) {
                    alert('奖品已申请');
                    return;
                };
                if(canApply2 == 1) {
                    // 调取中，不可重复调取
                    canApply2 = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ReceiveAward',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 2
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('level2prize', JSON.stringify(data.data.AdvancedPrize));
                                $('.level2').addClass('applyed');
                                $('.level2 .box-apply').removeClass('apply');
                                $('.level2 .box-apply img').attr('src', 'assets/images/prize/success-apply.png');
                            };
                        },
                        error: function() {
                            //
                            alert('申请失败，请重试');
                        },
                        complete: function(data) {
                            // 调取结束
                            canApply2 = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    }); 
                };
            });
            var canExchange2 = 1;
            $('.level2 .box-exchange.exchange').off('click');
            $('.level2 .box-exchange.exchange').on('click', function() {
                if(JSON.parse(localStorage.getItem('level2prizeEx'))) {
                    alert('奖品已兑换');
                    return;
                };
                if(canExchange2 == 1) {
                    // 调取中，不可重复调取
                    canExchange2 = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ExchangeAward',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 2
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('level2prizeEx', JSON.stringify(data.data.AdvancedExchangePrize));
                                $('.level2 .box-exchange').removeClass('exchange');
                                $('.level2 .box-exchange img').attr('src', 'assets/images/prize/success-exchange.png');
                            };
                        },
                        error: function() {
                            //
                            alert('兑换失败，请重试');
                        },
                        complete: function(data) {
                            // 调取结束
                            canExchange2 = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    }); 
                };
            });

            var canApply3 = 1;
            $('.level3.unlock .apply').off('click');
            $('.level3.unlock .apply').on('click', function() {
                if(JSON.parse(localStorage.getItem('level3prize'))) {
                    alert('奖品已申请');
                    return;
                };
                if(canApply3 == 1) {
                    // 调取中，不可重复调取
                    canApply3 = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ReceiveAward',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 3
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('level3prize', JSON.stringify(data.data.MaturePrize));
                                $('.level3').addClass('applyed');
                                $('.level3 .box-apply').removeClass('apply');
                                $('.level3 .box-apply img').attr('src', 'assets/images/prize/success-apply.png');
                            };
                        },
                        error: function() {
                            //
                            alert('申请失败，请重试');
                        },
                        complete: function(data) {
                            // 调取结束
                            canApply3 = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    }); 
                };
            });
            var canExchange3 = 1;
            $('.level3 .box-exchange.exchange').off('click');
            $('.level3 .box-exchange.exchange').on('click', function() {
                if(JSON.parse(localStorage.getItem('level3prizeEx'))) {
                    alert('奖品已兑换');
                    return;
                };
                if(canExchange3 == 1) {
                    // 调取中，不可重复调取
                    canExchange3 = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/ExchangeAward',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 3
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('level3prizeEx', JSON.stringify(data.data.MatureExchangePrize));
                                $('.level3 .box-exchange').removeClass('exchange');
                                $('.level3 .box-exchange img').attr('src', 'assets/images/prize/success-exchange.png');
                            };
                        },
                        error: function() {
                            //
                            alert('兑换失败，请重试');
                        },
                        complete: function(data) {
                            // 调取结束
                            canExchange3 = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    }); 
                };
            });

            // back modules
            $('.btn-modules').on('click', function() {
                tl.kill();
                app.router.goto('modules');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
