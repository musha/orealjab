// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'login');
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.login_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // check name
            function checkName(name) {
                if(name == '') {
                    alert('请输入姓名');
                    return false;
                }
                var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
                if (!reg.test(name)) {
                    alert('名字格式不正确');
                    return false;
                } else {
                    return true;
                }
            };

            // check number
            function checkNumber(num) {
                if(num == '') {
                    alert('请输入店铺编号');
                    return false;
                };
                var reg = /^\d{4}$/;
                if (!reg.test(num)) {
                    alert('请输入4位店铺编号');
                    return false;
                } else {
                    return true;
                }
            };

            // check pwd
            function checkPwd(pwd) {
                if(pwd == '') {
                    alert('请输入密码');
                    return false;
                };
                if(pwd.length > 0 && pwd.length < 6) {
                    alert('请输入6位密码');
                    return false;
                } else {
                    return true;
                }
            };

            // login
            var canLogin = 1; // 是否可以登录，默认可以
            $('.btn-con').off('click');
            $('.btn-con').on('click', function() {
                // console.log($('.userName').val());
                // console.log($('.userNumber').val());
                // console.log($('.userPwd').val());
                if(checkName($('.userName').val()) && checkNumber($('.userNumber').val()) && checkPwd($('.userPwd').val())) {
                    if(canLogin == 1) {
                        // 正在调接口，不可以重复调取
                        canLogin = 0;
                        $('.modal-loading').fadeIn('fast');
                        $.ajax({
                            type: 'post',
                            url: 'https://campaign.lorealparis.com.cn/jablearning/Server/Login',
                            dataType: 'json',
                            // contentType: 'application/json',
                            data: {
                                name: $('.userName').val(),
                                storeNum: $('.userNumber').val(),
                                passWord: $('.userPwd').val()
                            },
                            // async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);
                                if(data.msg != '') {
                                    if(!/成功+/.test(data.msg)) {
                                        alert(data.msg);
                                    };
                                };
                                if(data.status) {
                                    localStorage.setItem('userName', data.data.Name); // 存储用户名
                                    localStorage.setItem('userNum', data.data.StoreNum); // 存储店铺编号
                                    localStorage.setItem('studyStatus', JSON.stringify(data.data.StudyStatus)); // 存储学习状态
                                    localStorage.setItem('level1score', JSON.stringify(data.data.EntryLevelScore)); // 存储level1分数
                                    localStorage.setItem('level2score', JSON.stringify(data.data.AdvancedScore)); // 存储level2分数
                                    localStorage.setItem('level3score', JSON.stringify(data.data.MatureScore)); // 存储level3分数
                                    localStorage.setItem('level1prize', JSON.stringify(data.data.EntryLevelPrize)); // 存储level1奖品
                                    localStorage.setItem('level2prize', JSON.stringify(data.data.AdvancedPrize)); // 存储level2奖品
                                    localStorage.setItem('level3prize', JSON.stringify(data.data.MaturePrize)); // 存储level3奖品
                                    localStorage.setItem('level1prizeEx', JSON.stringify(data.data.EntryLevelExchangePrize)); // 存储level1奖品兑换状态
                                    localStorage.setItem('level2prizeEx', JSON.stringify(data.data.AdvancedExchangePrize)); // 存储level2奖品兑换状态
                                    localStorage.setItem('level3prizeEx', JSON.stringify(data.data.MatureExchangePrize)); // 存储level3奖品兑换状态
                                    // localStorage.setItem('percentA', JSON.stringify(data.data.AmaniRatio)); // 存储阿玛尼平均分
                                    // localStorage.setItem('percentB', JSON.stringify(data.data.BiothermRatio)); // 存储碧欧泉平均分
                                    // localStorage.setItem('percentL', JSON.stringify(data.data.LancomeRatio)); // 存储兰蔻平均分
                                    // localStorage.setItem('percentS', JSON.stringify(data.data.ShuUemuraRatio)); // 存储植村秀平均分
                                    tl.kill();
                                    app.router.goto('modules');
                                };
                            },
                            error: function() {
                                //
                                alert('登录失败，请重试');
                            },
                            complete: function(data) {
                                // 调取完毕
                                canLogin = 1;
                                $('.modal-loading').fadeOut('fast');
                            }
                        });
                    };
                };   
            });
            
            // reg
            $('.reg-box').on('click', function() {
                tl.kill();
                app.router.goto('reg');
            });

            // home
            $('.btn-home').on('click', function() {
                tl.kill();
                app.router.goto('home');
            }); 
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
});
