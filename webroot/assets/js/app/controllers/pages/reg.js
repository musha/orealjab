// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'reg');
            }; 

            // date
            $('.userDate').on('input propertychange', function() {
                if($(this).val() == '') {
                    $(this).addClass('show-placeholder');
                } else {
                    $(this).removeClass('show-placeholder');
                };
            });

            // fill city
            var cityId = '';
            var citySelect = new MobileSelect({
                trigger: '.userCity',
                title: '城市选择',
                wheels: [
                            {data:[
                                {
                                    id: '', 
                                    value: '',
                                    childs: [
                                        {id: '', value: ''},
                                    ]
                                }
                            ]}
                        ],
                transitionEnd:function(indexArr, data){
                    // console.log(data);
                },
                callback:function(indexArr, data){
                    cityId = data[0].id;
                    $('.userCity').text(data[1].value);
                    fillTeac(cityId);
                }
            });
            citySelect.updateWheels(app.city); // Update the wheel

            // fill teac
            function fillTeac(num) {
                $('.defaultOption').siblings().remove();
                if(num === '') {
                    return;
                };
                var len = app.teac[num].length;
                for(var i = 0; i < len; i++) {
                    $('.userTeac').append('<option value=' + app.teac[num][i] + '>' + app.teac[num][i] + '</option>');
                };
            };
            $('.userTeac').on('click', function() {
                if(cityId === '') {
                    alert('请先选择城市');
                };
            });
            
            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.reg_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                // 
            } }, 0.1);

            // check name
            function checkName(name) {
                if(name == '') {
                    alert('请输入姓名');
                    return false;
                }
                var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
                if (!reg.test(name)) {
                    alert('名字格式不正确')
                    return false;
                } else {
                    return true;
                }
            };

            // check city
            function checkCity(city) {
                if(city == '' || city == null || city == undefined) {
                    alert('请选择城市');
                    return false;
                } else {
                    return true;
                }
            };

            // check number
            function checkNumber(num) {
                if(num == '') {
                    alert('请输入店铺编号');
                    return false;
                };
                var reg = /^\d{4}$/;
                if (!reg.test(num)) {
                    alert('请输入4位店铺编号');
                    return false;
                } else {
                    return true;
                }
            };

            // check date
            function checkDate(date) {
                if(date == '') {
                    alert('请选择日期');
                    return false;
                } else {
                    return true;
                }
            };

            // check teac
            function checkTeac(teac) {
                if(teac == '') {
                    alert('请选择培训师');
                    return false;
                } else {
                    return true;
                };
            };

            // check pwd
            function checkPwd(pwd) {
                if(pwd == '') {
                    alert('请输入密码');
                    return false;
                };
                if(pwd.length > 0 && pwd.length < 6) {
                    alert('请输入6位密码');
                    return false;
                } else {
                    return true;
                }
            };

            // check pwd
            function checkPwdcon(pwd) {
                if(pwd == '') {
                    alert('请确认密码');
                    return false;
                };
                if(pwd !== $('.userPwd').val()){
                    alert('密码不一致');
                    return false;
                } else {
                    return true;
                }
            };

            // reg
            var canReg = 1; // 是否可以登录，默认可以
            $('.btn-reg').off('click');
            $('.btn-reg').on('click', function() {
                // console.log($('.userName').val());
                // console.log(citySelect.getValue()[1].value);
                // console.log($('.userNumber').val());
                // console.log($('.userDate').val());
                // console.log($('.userTeac').val());
                // console.log($('.userPwd').val());
                // console.log($('.userPwdcon').val());
                if(checkName($('.userName').val()) && checkCity(citySelect.getValue()) && checkNumber($('.userNumber').val()) && checkDate($('.userDate').val()) && checkTeac($('.userTeac').val()) && checkPwd($('.userPwd').val()) && checkPwdcon($('.userPwdcon').val())) {
                    if(canReg == 1) {
                        // 正在调接口，不可以重复调取
                        canReg = 0;
                        $('.modal-loading').fadeIn('fast');
                        $.ajax({
                            type: 'post',
                            url: 'https://campaign.lorealparis.com.cn/jablearning/Server/Register',
                            dataType: 'json',
                            // contentType: 'application/json',
                            data: {
                                name: $('.userName').val(),
                                city: citySelect.getValue()[1].value,
                                storeNum: $('.userNumber').val(),
                                dateEntry: $('.userDate').val(),
                                trainer: $('.userTeac').val(),
                                passWord: $('.userPwd').val(),
                            },
                            // async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);
                                if(data.msg != '') {
                                    // alert(data.msg);
                                };
                                if(data.status) {
                                    $('.modal-success').fadeIn('fast');
                                };
                            },
                            error: function() {
                                //
                                alert('注册失败，请重试');
                            },
                            complete: function(data) {
                                // 调取完毕
                                canReg = 1;
                                $('.modal-loading').fadeOut('fast');
                            }
                        });
                    };
                };
            });

            // to login
            $('.btn-login').on('click', function() {
                tl.kill();
                app.router.goto('login');
            });

            // back
            $('.btn-back').on('click', function() {
                tl.kill();
                app.router.goto('login');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
