// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'test3');
            };

            if(JSON.parse(localStorage.getItem('level3score')) == 0) {
                // 要答所有题目
                $('.qa-item').addClass('show');
            } else {
                // 调取错过的题目
                $('.modal-loading').fadeIn('fast');
                $.ajax({
                    type: 'post',
                    url: 'https://campaign.lorealparis.com.cn/jablearning/server/GetWrongQusetions',
                    dataType: 'json',
                    // contentType: 'application/json',
                    data: {
                        name: localStorage.getItem('userName'),
                        storeNum: localStorage.getItem('userNum'),
                        level: 3
                    },
                    // async: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        if(data.msg != '') {
                            if(/未登录+/.test(data.msg)) {
                                localStorage.removeItem('userName');
                                localStorage.removeItem('userNum');
                                tl.kill();
                                app.router.goto('login');
                                return;
                            };
                        };
                        if(data.status) {
                            var arr = data.WrongQuestion.split(','),
                                len = arr.length;
                            if(len > 0) {
                                for(var i = 0; i < len; i++) {
                                    $('.qa-item' + arr[i]).addClass('show');
                                };
                            };
                        };
                    },
                    error: function() {
                        //
                        alert('网络错误，请刷新重试');
                    },
                    complete: function(data) {
                        // 调取完毕
                        $('.modal-loading').fadeOut('fast');
                    }
                });
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.test3_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // 单选题

            // 验证是否完成
            function checkA(el, num) {
                if(el.parents('.qa-item').hasClass('show')) {
                    if(el.find('input:checked').length == 0) {
                        alert('请完成第' + num + '题');
                        return false;
                    } else {
                        return true;
                    };
                } else {
                    // skip
                    return true;
                };
            };

            // submit
            var canSubmit = 1; // 默认可以提交
            $('.test3_page .btn-submit').off('click');
            $('.test3_page .btn-submit').on('click', function() {
                if(checkA($('.a1'), 1) && checkA($('.a2'), 2) && checkA($('.a3'), 3) && checkA($('.a4'), 4) && checkA($('.a5'), 5)) {
                    var len = $('.qa-item.show').length,
                        answerO = {}, // 需要提交的答案
                        wrongAn = [], // 错题组
                        score = 0; // 分数
                    for(var i = 0; i < len; i++) {
                        // 显示的题目
                        answerO['an' + $($('.qa-item.show')[i]).data('index')] = $($('.qa-item.show')[i]).find('input:checked').attr('id').charAt(0);
                        if($($('.qa-item.show')[i]).find('input:checked').attr('id').charAt(0) == 'N') {
                            wrongAn.push($($('.qa-item.show')[i]).data('index'));
                        } else {
                            score += 20;
                        }
                    };
                    answerO['score'] = score;
                    if(wrongAn.length > 0) {
                        answerO['WrongQuestion'] = wrongAn.join(',');
                    };
                    // console.log(wrongAn);
                    // console.log(answerO);
                    // can submit
                    if(canSubmit == 1) {
                        // 调用中不能再次提交
                        canSubmit = 0;
                        $('.modal-loading').fadeIn('fast');
                        $.ajax({
                            type: 'post',
                            url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetScore',
                            dataType: 'json',
                            // contentType: 'application/json',
                            data: {
                                name: localStorage.getItem('userName'),
                                storeNum: localStorage.getItem('userNum'),
                                level: 3,
                                answer: JSON.stringify(answerO)
                            },
                            // async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);
                                if(data.msg != '') {
                                    // alert(data.msg);
                                    if(/未登录+/.test(data.msg)) {
                                        localStorage.removeItem('userName');
                                        localStorage.removeItem('userNum');
                                        tl.kill();
                                        app.router.goto('login');
                                        return;
                                    };
                                };
                                if(data.status) {
                                    localStorage.setItem('studyStatus', JSON.stringify(data.data.StudyStatus)); // 存储学习状态
                                    localStorage.setItem('level3score', JSON.stringify(data.score)); // 更新分数
                                    if(wrongAn.length > 0) {
                                        // 有选no的
                                        sessionStorage.setItem('rightNum3', JSON.stringify(len - wrongAn.length)); // 存储对题数
                                        sessionStorage.setItem('wrongNum3', JSON.stringify(wrongAn.length)); // 存储错题数
                                    } else {
                                        sessionStorage.setItem('rightNum3', JSON.stringify(len)); // 存储对题数
                                        sessionStorage.setItem('wrongNum3', JSON.stringify(0)); // 存储错题数
                                    };
                                    sessionStorage.setItem('errorArr3', JSON.stringify(wrongAn)); // 存储no题组
                                    tl.kill();
                                    app.router.goto('analyse3');
                                };
                            },
                            error: function() {
                                //
                                alert('提交失败，请重试');
                            },
                            complete: function(data) {
                                //
                                canSubmit = 1;
                                $('.modal-loading').fadeOut('fast');
                            }
                        });
                    };
                };
            });

            // back
            $('.btn-back').on('click', function() {
                tl.kill();
                app.router.goto(context.args[0]);
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
